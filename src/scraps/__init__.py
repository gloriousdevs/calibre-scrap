
def get_iter(arg):
    
    if ":" not in arg:
        return None
    
    scrap, query = arg.split(":",1)


    if scrap == "openlibra":
        import openlibra
        it_ = openlibra.openlibra_iter(query)
    
    elif scrap == "openlibra_api":
        import openlibra_api
        it_ = openlibra_api.openlibra_api_iter(query)

    else:

        it_ = None

    return scrap, it_
