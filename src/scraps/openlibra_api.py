#-*- coding: utf-8 -*-
'''
Created on 23/11/2011

@author: jmcortesa
'''

import re, urllib, urllib2
from datetime import date, datetime

import json
from calibre import prints
from calibre.ebooks.BeautifulSoup import BeautifulSoup
from calibre.ebooks.metadata import MetaInformation
from calibre.utils.localization import langnames_to_langcodes

from openlibra import parse_url_wrap

MAXITEMS=500 #10000 #max number of books to retrieve

#DOC: http://www.etnassoft.com/api-documentacion/
#API = "http://openlibra.com/api/v1/get/?%s"
API = "http://www.etnassoft.com/api/v1/get/?%s"

def mk_url(**kwargs):
    
    if "num_items" not in kwargs:
        kwargs["num_items"] = MAXITEMS

    url = API%urllib.urlencode(kwargs)
#    url = url.replace("since=","")

    return url

def openlibra_api_iter(qs):

    pat = "(today|last_week|last_month|last_year)\+?"
    m = re.search(pat, qs)

    if m is not None:

        url = mk_url(since=m.group(1))
        txt = urllib2.urlopen(url)\
                .read()\
                .replace("\n","")[1:-2]
        js = json.loads(txt)

    else:

        pat = "(?P<nfrom>\d+)(?:-(?P<nto>\d+))?\+?$"
        m = re.search(pat, qs)

        if m is None:
            return None

        nfrom, nto = m.groups()

        nfrom = int(nfrom)
        nto = int(nto) if nto is not None else nfrom

        js = range_js(nfrom, nto)

  
    # Se podría pedir a la API un rango de ids, pero las URLs devueltas
    # tiene caducidad, por lo que no queda otro remedio que pedir libro a libro
    id_list = (j["ID"] for j in js)
    parser = meta_json_plus if qs.endswith("+") else meta_json
    it_ = (parser(get_js(n)) for n in id_list)

    return it_

def get_js(n_id):
    
    url = mk_url(id=n_id, num_items=1)
    txt = urllib2\
            .urlopen(url)\
            .read()\
            .replace("\n","")[1:-2]
    js = json.loads(txt)
  
    return js[0] if js else None


def range_js(nfrom, nto):

    rang = (get_js(n) for n in xrange(nfrom, nto+1))

    return (js for js in rang if js)


def meta_json(js):

    mi = MetaInformation(None, None)
    mi.identifiers = {"openlibra":str(js["ID"])}

    mi.title = js["title"]
    mi.authors_from_string(js["author"])
    mi.pubdate = datetime(int(js["publisher_date"]), 1, 1)
    mi.publisher = js["publisher"]

    lang = js["language"]
    if lang:
        lang = lang.capitalize()
        mi.languages = [lang]
#    mi.languages = langnames_to_langcodes([details[u"Idioma"]]).values()

    mi.tags = [t["name"] for t in js["tags"]]+[c["name"] for c in js["categories"]]

    #TODO: La API no ofrece la descripción completa
    #tan sólo la url donde está la descripción
    url = js["url_details"]
    mi.comments = '<hr/>REF:<a href="%s">%s</a>'%(url, url)

    mi.rating = float(js["rating"])

    download = js["url_download"]
    read_online = js["url_read_online"]
    frontcover = js["cover"]

    #all url must be utf8-encoded
    download = download.encode("utf8")
    frontcover = frontcover.encode("utf8")

    return (mi, download, frontcover, read_online)

def meta_json_plus(js):
    
    _id = js["ID"]
    (mi, _, _, _) = parse_url_wrap(int(_id))
    comments = mi.comments
    
    (mi, download, frontcover, read_online) = meta_json(js)
    mi.comments = comments
    
    return (mi, download, frontcover, read_online)


def parser_json(num):

    url = API%urllib.urlencode({"id":num})

    js = json.loads(urllib2.urlopen(url).read()[1:-2])[0]

    return meta_json(js)


