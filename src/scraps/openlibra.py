#-*- coding: utf-8 -*-
'''
Created on 23/11/2011

@author: jmcortesa
'''

import re, urllib2
from datetime import datetime

import json
from calibre import prints
from calibre.ebooks.BeautifulSoup import BeautifulSoup
from calibre.ebooks.metadata import MetaInformation
from calibre.utils.localization import langnames_to_langcodes


WEB="http://www.etnassoft.com/?page_id=%d"

pat_js=re.compile("<script.*?</script>", re.M|re.DOTALL)
pat_stars=re.compile("ratings_off\((\d+(?:\.\d+)?)")

def openlibra_iter(qs):

    pat="(?P<nfrom>\d+)(?:-(?P<nto>\d+))?$"

    m=re.search(pat, qs)

    if m is None:
        return None

    nfrom, nto=m.groups()

    nfrom=int(nfrom)
    nto=int(nto) if nto is not None else nfrom

    return (parse_url_wrap(n) for n in xrange(nfrom, nto+1))

def parse_url_wrap(num):

    try:
        res=parse_url(num)
        prints(num, res[0].title)
    except:
        prints(num, "##No existe##")
        res=None
        
    return res

def parse_url(num):
    url=WEB%num

    u=urllib2.urlopen(url)
    url=u.geturl()
    doc=u.read()
    doc=pat_js.sub("", doc) #evitar posible errores de parseo con javascript

    soup=BeautifulSoup(doc,
                         smartQuotesTo=None,
                         convertEntities=BeautifulSoup.HTML_ENTITIES)

    mi=MetaInformation(None, None)
    mi.identifiers={"openlibra":str(num)}

    rows=soup.find("div", "book_details").findAll("div", "row")
    labels = (row.find("div","label").string[:-1] for row in rows)
    values = (row.find("div","value").string.strip() for row in rows)
    details=dict(zip(labels, values))

    mi.title=details[u"Título"]
    mi.authors_from_string(details[u"Autor(es)"])
    mi.pubdate=datetime(int(details[u"Publicación"]), 1, 1)
    mi.publisher=details[u"Editorial"]

    #===========================================================================
    # idioma = details[u"Idioma"]
    # langs = langnames_to_langcodes([idioma])
    # mi.languages = langs.values()
    #===========================================================================
    lang = details[u"Idioma"]
    if lang:
        mi.languages = [lang]

#    mi.languages=langnames_to_langcodes([details[u"Idioma"]]).values()

    mi.tags=list(set(li.a.string.lower() 
                    for div in soup.findAll("div", "book_tags") if div.find("ul")
                        for li in div.ul ))

    ### Descripción

    #Si sólo hubiera párrafos:
    #mi.comments = "".join(unicode(p) for p in soup.find("div", "book_content").findAll("p"))

    content=soup.find("div", "book_content")
    #Eliminar los divs insertados
    for div in content.findAll("div"):
        div.extract()
    mi.comments="".join(unicode(t) for t in content.contents)
    mi.comments+='<hr/>REF:<a href="%s">%s</a>'%(url, url)

    ###

    stars=soup.find("img", onmouseout=pat_stars)
    if stars:
        rating=pat_stars.search(stars["onmouseout"]).group(1)
        mi.rating=float(rating)*2  #de 0 a 10

    download_books=soup.findAll("div", attrs={"class":re.compile(r"\bdownload_book\b")})
    download=download_books[0].a["href"]
    read_online=download_books[1].a["href"]  #online reading
    frontcover=soup.find("div", "frontcover").img["src"]

    #all url must be utf8-encoded
    download=download.encode("utf8")
    frontcover=frontcover.encode("utf8")

    return (mi, download, frontcover, read_online)
