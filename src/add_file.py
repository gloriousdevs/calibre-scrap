'''
Created on 23/11/2011

@author: jmcortesa
'''

import os
import urllib
from zipfile import ZipFile

from calibre import prints
from calibre.library import cli
from calibre.ptempfile import PersistentTemporaryDirectory
from calibre.ptempfile import PersistentTemporaryFile

from calibre.utils import unrar

def add_file(db, id_, download):

    #prints("FILE: %s"%download)

    stream=urllib.urlopen(download)
    ext=os.path.splitext(download)[1].lower()
    if len(ext)>4:
    	ext=ext[:4]

    zfile=PersistentTemporaryFile(suffix=ext)
    zfile.write(stream.read())
    zfile.close()

    if ext in (".zip", ".rar"):

        if ext==".zip":

            class zZipFile(object):
                def __init__(self, path):
                    self.path=path
                    self.zf=ZipFile(path, 'r')
                    self.names=self.namelist()
                def namelist(self):
                    return list(fname for fname in self.zf.namelist()
                                    if '.' in fname)
                def extractor(self, fname):
                    return self.zf.read(fname)

            try:
                zf=zZipFile(zfile.name)
            except:
                prints("##Not a ZipFile##:", download)
                return False

        elif ext==".rar":

            class zRarFile(object):
                def __init__(self, path):
                    self.path=path
                    self.names=self.namelist()
                def namelist(self):
                    with open(self.path,'rb') as stream:
                        filelist=list(fname for fname in unrar.names(stream)
                                if '.' in fname)
                    return filelist
                def extractor(self, fname):
                    with open(self.path,'rb') as stream:
                        _, data=unrar.extract_member(stream, name=fname)
                    return data
            zf=zRarFile(zfile.name)

        for fname in zf.names:
            prints("Adding:", fname)
            ext=os.path.splitext(fname)[1]
            tmpfile=PersistentTemporaryFile(suffix=ext)
            tmpfile.write(zf.extractor(fname))
            tmpfile.close()
            db.add_format_with_hooks(id_, ext[1:], tmpfile.name, index_is_id=True)
	    cli.send_message()

    elif ext in (".pdf", ".chm"):

        fname=zfile.name

        prints("Adding:", fname)
        db.add_format_with_hooks(id_, ext[1:], fname, index_is_id=True)
	cli.send_message()

    else:
        prints("File extension '%s' not implemented"%ext)
        return False

    return True

