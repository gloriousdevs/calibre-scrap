'''
Created on 23/11/2011

@author: jmcortesa
'''

import sys
import re, urllib
import time
import threading
from Queue import Queue

from calibre import prints
from calibre.library import db, cli

from add_file import add_file
import scraps


def get_params(arg):

    pat="(?P<scrap>.+?):(?P<nfrom>\d+)(?:-(?P<nto>\d+))?$"

    m=re.search(pat, arg)

    if m is None:
        usage()
        sys.exit()

    scrap, nfrom, nto=m.groups()

    nfrom=int(nfrom)
    nto=int(nto) if nto is not None else nfrom

    return scrap, nfrom, nto

def usage():
    runscript=sys.argv[0]
    prints("Usage: %s -- <scrap-method>:<range> [<library-path>]"%runscript)
    prints("       <scrap-method> -- openlibra")
    prints("       <range> -- from-to range")

def do_qmeta(db, qmeta, qdownloads):
    while True:
        mi, download, cover, read_online=qmeta.get()
        id_=db.create_book_entry(mi, cover=urllib.urlopen(cover),
                                    add_duplicates=False)
        if id_:
            qdownloads.put((id_, download, read_online))
        else:
            prints("### Duplicated book:", mi.title)

        qmeta.task_done()

def do_qdownloads(db, qdownloads):
    while True:
        id_, download, read_online=qdownloads.get()
        doit=add_file(db, id_, download)
        if not doit:
            prints("Trying with online version")
            add_file(db, id_, read_online)
        qdownloads.task_done()

def do_main(book, qmeta):

    if book is not None:
        mi, download, cover, read_online=book
        qmeta.put((mi, download, cover, read_online))


def main(scrap_iter, library_path=None):

    calibredb=db(library_path)

    qmeta=Queue()
    qdownloads=Queue()

    #hilo para insertar el libro en calibre
    p=threading.Thread(target=do_qmeta, args=(calibredb, qmeta, qdownloads))
    p.daemon=True
    p.start()

    #hilos para descargar ficheros
    for i in xrange(3):
        p=threading.Thread(target=do_qdownloads, args=(calibredb, qdownloads))
        p.daemon=True
        p.start()

    #hilos principales de scraping
    for book in scrap_iter:
        p=threading.Thread(name="scrap", target=do_main, args=(book, qmeta))
        p.start()

        #si hay muchos hilos, frenamos un poco
        if threading.active_count()>16:
            p.join()

    #esperamos a que terminen los hijos
    prints("### Esperando para concluir...")
    while any(p for p in threading.enumerate()
                            if p.name.startswith("scrap")):
        time.sleep(2)

    qmeta.join()
    qdownloads.join()


    cli.write_dirtied(calibredb)
    cli.send_message()


if __name__=='__main__':

    if len(sys.argv)<2:
        usage()
        sys.exit()

    scrap,scrap_iter=scraps.get_iter(sys.argv[1])

    if scrap_iter is None:
        prints("Error with parser %s"%scrap)
        usage()
        sys.exit()

    library_path = None if len(sys.argv)<3 else sys.argv[2]

    main(scrap_iter, library_path)
