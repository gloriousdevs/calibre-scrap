# scraping con calibre

> NOTA:
>
> Los últimos cambios en la aplicación de calibre y, sobre todo, en la remodelada web (en fase beta) de openlibra están teniendo un enorme impacto en el funcionamiento habitual de `calibre-scrap`.
>
> Unido a esto, las nuevas maneras de usar concurrencia con `Futures` y `asyncio`, así como otras novedades introducidas en python3 que algún día tendrá que adoptar calibre, hace muy difícil seguir manteniendo este proyecto como hasta ahora.
>
> Este proyecto necesita un replanteamiento muy en serio, por lo que he decidido que resulta mejor darlo por finalizado para volverlo a empezar más adelante, cuando vea el momento adecuado.
>
> FIN

---

Este proyecto pretende usar calibre como entorno de ejecución de scripts de
_scraping_ con el objeto de extraer información de libros electrónicos disponibles
en distintas webs e introducir esta información en la base de datos de calibre.

Como primer ejemplo de _scraping_ tenemos la web de [OpenLibra][] <http://openlibra.com>

## Uso

Asegúrate tener instalado calibre y ábrete una biblioteca (recomendable crear una nueva para
mayor seguridad). Descarga la carpeta de scripts **calibre-scrap**, entra dentro de la carpeta
`src` y ejecuta el siguiente comando:

```bash
    calibre-debug run_scrap.py -- <scrap-method>:<query> [<library-path>]
```

Donde `<library-path>` es la ruta a la biblioteca. Si no se da una, tomará la última biblioteca que se haya usado.

Los métodos `<scrap-method>` pueden ser:

### openlibra

Scrap normal desde html. Es más lento, pero añade las descripciones que no aparecen usando el método `openlibra_api` siguiente.

#### `<nfrom\>-<nto>`

Especifica los libros a descargar como un rango de identificadores. Este identificador
aparecerá como uno más dentro de los metadatos del libro en calibre.

En muchos casos no existirá correspondencia con ningún libro, por lo que es mejor que pruebes primero con rangos pequeños.

Ejemplos

* Para descargar los libros desde el ID 2000 al ID 2100:

```bash
    calibre-debug run_scrap.py -- openlibra:2000-2100
```

### openlibra_api

Usa la [API de openlibra](http://www.etnassoft.com/api-documentacion/) que funciona a través de JSON. Todos los
campos devueltos son incorporados a calibre, aunque no facilita ningún campo para la "descripción".

#### `<nfrom>-<nto>`

Especifica los libros a descargar como un rango de identificadores. Este identificador como uno más dentro de los metadatos del libro en calibre.

Comprueba que exista el libro, por lo que es mucho más rápido que el método `openlibra`. Si se añade al final un signo `+`, entonces emplea el scrap `openlibra` en lugar de los datos devueltos por JSON (es la única forma de obtener también las descripciones)

#### `<today|last_week|last_month|last_year>`

Incorpora un rango de las novedades del día, última semana, último mes o último año, respectivamente.

Añadiendo un signo `+` al final se emplea el scrap `openlibra` (única forma de obtener las descripciones)

Ejemplos:

* Para descargar los libros desde el ID 2000 al ID 2100:

```bash
    calibre-debug run_scrap.py -- openlibra_api:2000-2100
```

* Para descargar los libros desde el ID 2000 al ID 2100, pero usando el scrap `openlibra`:

```bash
    calibre-debug run_scrap.py -- openlibra_api:2000-2100+
```

* Para descargar las novedades del último mes, usando el scrap `openlibra`:

```bash
    calibre-debug run_scrap.py -- openlibra_api:last_month+
```

## Próximamente

Pronto estarán disponibles nuevos _scrapings_ para otras webs.

Estudiando la posibilidad de crear un _"plugin"_ para calibre que descargue libros de [openlibra][].

## Desarrollo

**calibre** incluye gran cantidad de herramientas junto con el intérprete de python empotrado.
La idea de estos scripts de scraping es la de aprovecharse de toda esta infraestructura
multiplataforma para el mantenimiento de bibliotecas de ebooks.

## Truco

Si quieres tener una columna con el ID de openlibra, basta con crear una columna calculada con el siguiente texto:

```plain
    Lookup name: identifiers
    Column heading: OpenlibraID
    Template: {identifiers:select(openlibra)}
    Sort column by: Number
```

[openlibra]: http://openlibra.com "OpenLibra"
